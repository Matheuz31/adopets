import { Button, Col, Layout, Row, Typography } from 'antd';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import { Redirect, Route, Switch } from "react-router-dom";
import AuthService from '../domains/Auth/Service/AuthService';
import ProductForm from '../pages/Dashboard/Product/ProductForm';
import ProductList from '../pages/Dashboard/Product/ProductList';
import { setToken } from '../services/Http';

declare const window: any;
const { Header, Content} = Layout;
const { Title } = Typography;

const Dashboard = (props:any) => {
    const history = useHistory();
    const service = new AuthService()
    const user = window.$user    

    function signOut () {
        service.signOut()
            .then( (res:any) => {
                setToken('')
                history.replace('/auth/signin')
            })
        
    }

    return (
        <Layout>
          <Layout>
            <Header className="siteLayoutBackground" style={{padding: 0, background: "#001529"}}>
                <Row justify="end" align="middle" >
                    <Col span={6}>
                        <Title style={{textAlign: 'center', color: 'white'}} level={4}>
                        {user}
                        </Title>
                    </Col>
                    <Col span={4}><Button danger onClick={signOut} >Sign Out</Button></Col>
                </Row>
            </Header>
              <Content style={{margin: '24px 16px', padding: 24, minHeight: "calc(100vh - 114px)", background: "#fff"}}>
                <Switch>
                    <Route exact path="/dashboard/products" component={ProductList} />
                    <Route path="/dashboard/products/add" component={ProductForm} />
                    <Route path="/dashboard/products/:id" component={ProductForm} />
                    <Redirect from="/dashboard/*" to="/auth/signin"  />
                </Switch>
              </Content>
          </Layout>
        </Layout>
    )
}

export default Dashboard