import { setToken, updateUser } from "../services/Http"

export default async () => {
    const token = await localStorage.getItem('token')
    
    if (token && typeof token === 'string') {
        setToken(token)
    }
    
    const user = await localStorage.getItem('user')

    if (user && typeof user === 'string') {
        updateUser(user)
    }
}