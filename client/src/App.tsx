import React from 'react';
import Routes from './router/Routes'
import './App.css';
import boot from './boot'

const App = () => {

  boot()

  return (
    <Routes />
  );
}

export default App;
