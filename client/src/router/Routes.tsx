import React from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import SignIn from '../pages/Auth/SignIn'
import SignUp from '../pages/Auth/SignUp'
import Dashboard from '../layouts/Dashboard'

const Routes = () => {
    return (
        <Router >
            <Switch>
                <Route exact path="/auth/signup" component={SignUp} />
                <Route exact path="/auth/signin" component={SignIn} />
                <Route path="/dashboard" component={Dashboard} />
                <Redirect from="*" to="/auth/signin" />
            </Switch>
        </Router>
    )
}

export default Routes