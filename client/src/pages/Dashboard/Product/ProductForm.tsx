import { Button, Col, Form, Input, InputNumber, message, Row, Select, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useParams } from "react-router-dom";
import ProductService from '../../../domains/Product/Service/ProductService';
import CategoryModal from './components/CategoryModal';
import OptionsCategory from './components/OptionsCategory';
import { notificationError } from '../../../services/notification'

const { Title } = Typography;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};

const ProductForm = () => {
  const [form] = Form.useForm();
  const [loading] = useState(false);
  const [modal, setModal] = useState(false);
  
  const service = new ProductService()
  const { id } = useParams()
  const history = useHistory();

  const submit = (fields: any) => {
    if (id) {
      service.update({id, ...fields})
      .then((res: any) => {
        message.success(res.message);
      })
      .catch(notificationError)
    } else {
      service.create(fields)
      .then((res: any) => {
        message.success(res.message);
        setForm({ price: 0, stock: 0 })
      })
      .catch(notificationError)
    }
  };

  function goToList() {
    history.push('/dashboard/products')
  }

  function setForm(
    { name, description, price, stock, category_id }:{name?: '', description?: '', price?: 0, stock?: 0, category_id?: ''}) {
    form.setFieldsValue({
      name: name,
      description: description,
      price: price,
      stock: stock,
      category_id: category_id
    });
  }

  function changeStateModal (state:boolean) {
    setModal(state)
  }

  useEffect(() => {
    if (id) {
      service.read({ id })
        .then((res: any) => res.data)
        .then((res: any) => {
          const { category: category_id, ...data } = res
          setForm({...data, category_id});
        })
    } else {
      setForm({ price: 0, stock: 0 });
    }
  }, [])

  return (
    <div >
      <Row gutter={[40, 0]}>
        <Col span={14}>
          <Title style={{textAlign: 'right'}} level={2}>
            Product
          </Title>
        </Col>
        <Col span={7} >
          <Button htmlType="button" onClick={() => { setModal(true) }} >Add Category</Button>
        </Col>
      </Row>
      <Row gutter={[40, 0]}>
        <Col span={18}>
          <Form {...layout} form={form} onFinish={submit} >
            <Form.Item name="name" label="Name"
            rules={[
              {
                required: true,
                message: 'Product Name',
              },
              { min: 3, max: 30 }
            ]}
            >
              <Input size="large" placeholder="Enter the product name" />
            </Form.Item>
            <Form.Item name="description" label="Description" 
            rules={[ { max: 255 }]} >
              <Input.TextArea />
            </Form.Item>
            <Form.Item name="category_id" label="Category"
            rules={[
              {
                required: true,
                message: 'Category is required'
              }]} >
              <Select  >
                  {OptionsCategory()}
              </Select>
            </Form.Item>
            <Form.Item name="price" label="Price"
            rules={[
              {
                required: true,
                message: 'Price is required'
              },
              { type: 'number', min: 0 }
            ]}
            >
              <InputNumber size="large" placeholder="0.00" prefix="$" />
            </Form.Item>
            <Form.Item name="stock" label="Stock Available" 
            rules={[
              {
                required: true,
                message: 'Stock is required'
              },
              { type: 'number', min: 0 }
            ]}
            >
              <InputNumber size="large" />
            </Form.Item>
            <div style={{textAlign: "center"}}>
              <Button type="primary" loading={loading} htmlType="submit">
                Save
              </Button>{' '}
              <Button htmlType="button" onClick={goToList} >
                Back
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
      <CategoryModal modal={modal} changeState={changeStateModal} />
    </div>
  );
}

export default ProductForm