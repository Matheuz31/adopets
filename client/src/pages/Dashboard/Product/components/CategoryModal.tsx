import React from 'react'
import { Form, Input, Modal, message } from 'antd'
import CategoryService from '../../../../domains/Category/Service/CategoryService';

const CategoryModal = (props:any) => {
    const service = new CategoryService()
    const [form] = Form.useForm();

    function modalOk (values: object) {
        service.create(values)
            .then((res:any) => {
                message.success(res.message)
                modalClose()
            })
    }

    function modalCancel () {
        modalClose()
    }

    function modalClose () {
        form.setFieldsValue({ name: '' })
        props.changeState(false)
    }

    return (
        <Modal
        title="Add Categort"
        visible={props.modal}
        onCancel={modalCancel}
        onOk={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields();
                modalOk(values);
              })
              .catch(info => {
                console.log('Validate Failed:', info);
              });
          }}
        >
            <Form layout="vertical" form={form} onFinish={modalOk} name="form_in_modal" >
                <Form.Item name="name" label="Name"
                rules={[
                {
                    required: true,
                    message: 'Product Name',
                },
                { min: 3, max: 30 }
                ]}
                >
                    <Input size="large" placeholder="Enter the category name" />
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default CategoryModal