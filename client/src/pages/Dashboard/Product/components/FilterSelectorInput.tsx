import React from 'react'
import { Input, Select } from 'antd'
import OptionsCategory from './OptionsCategory';

const FilterSelectorInput = (props: any) => {

    if (!props.isselect()) {
        return (<Input {...props} style={{ width: '40%' }} />)
    } else {
        return (<Select {...props}  style={{ width: '40%' }} >
                    {OptionsCategory()}
               </Select>)
    }
}

export default FilterSelectorInput