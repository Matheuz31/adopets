import React, { useState, useEffect } from 'react'
import { Select } from 'antd';
import CategoryService from '../../../../domains/Category/Service/CategoryService';

const OptionsCategory = () => {
    const { Option } = Select;
    const [opts, setOpts] = useState([])
    const service = new CategoryService()

    useEffect(() =>  {
        service.all()
            .then((res:any) => res.data)
            .then((res:any) => {
                setOpts(res)
            })

    }, []);

    return (
        <>
            {opts.map((item:any) => <Option value={item.id}>{item.name}</Option>)}
        </>
    )

}

export default OptionsCategory