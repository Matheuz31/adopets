import { SearchOutlined } from '@ant-design/icons';
import { Button, Col, Input, message, Popconfirm, Row, Select, Table, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import ProductService from '../../../domains/Product/Service/ProductService';
import FilterSelectorInput from './components/FilterSelectorInput';

const { Title } = Typography;
const { Option } = Select;

const ProductList = () => {
    const history = useHistory();
    const [productsData, setProductsData] = useState([]);
    const [pagination, setPagination] = useState({ current: 1, pageSize: 2, total: 0 });
    const [inputSearch, setInputSearch] = useState('')
    const [selectSearch, setSelectSearch] = useState('name')
    const [loading] = useState(false);    
    const service = new ProductService();
    
    const columns = [
        {
          title: 'Name',
          dataIndex: 'name'
        },
        {
          title: 'Description',
          dataIndex: 'description'
        },
        {
            title: 'Category',
            dataIndex: 'category'
        },
        {
          title: 'Price',
          dataIndex: 'price'
        },
        {
          title: 'Stock',
          dataIndex: 'stock'
        },
        {
            title: 'Action',
            dataIndex: '',
            key: 'id',
            render: (props:any) => <div onClick={(e:any) => { e.stopPropagation() }} >
                                    <Popconfirm
                                        title="Are you sure delete this product?"
                                        onConfirm={ () => deleteProduct(props)}
                                        okText="Yes"
                                        cancelText="No"
                                    >
                                        <a>Delete</a>
                                    </Popconfirm>
                                </div>
        }
      ];
    

    useEffect(() =>  {
        searchTerms()
    }, []);

    const search = (page: number, column: string, term = '') => {
        service.search(page, column, term)
            .then((res: any) => {
                const { data, meta } = res
                
                setProductsData(data);
                setPagination({ current: meta.current_page, pageSize: meta.per_page, total: meta.total });
            })
    }

    function searchTerms() {        
        search(pagination.current, selectSearch, inputSearch)
    }

    function searchNewTerms () {
        search(1, selectSearch, inputSearch)
    }

    function onTableChange (pagination: any) {
        search(pagination.current, selectSearch, inputSearch)
    }

    function onClickAdd () {
        history.push('/dashboard/products/add')
    }

    function onChangeInput(e:any) {
        let val = e
        if (selectSearch !== 'category_id') {
            val = e.target.value        
        }
        
        setInputSearch(val)
    }

    function onChangeSelect(value:string) {
        setSelectSearch(value)
        
        if (selectSearch !== 'category_id') {
            onChangeInput({ target: { value: '' } })
        } else {
            onChangeInput('')
        }
    }

    function deleteProduct (row:any) {
        service.destroy(row)
        .then((res:any) => {
            message.success(res.message);
            searchNewTerms()
        });
        
    }

    function openProduct ({ id }: { id: number }) {
        history.push(`/dashboard/products/${id}`)
    }

    return (
        <div >
            <Row gutter={[20, 0]}>
                <Col span={6}>
                    <Title level={3}>
                        Product List
                    </Title>
                </Col>
                <Col span={10}>
                    <Input.Group compact>
                        <Select defaultValue="name" value={selectSearch} onChange={onChangeSelect}  style={{ width: '40%' }} >
                            <Option value="name">Name</Option>
                            <Option value="description">Description</Option>
                            <Option value="category_id">Category</Option>
                        </Select>
                        <FilterSelectorInput isselect={() => selectSearch === 'category_id'} onChange={onChangeInput}/>
                        <Button icon={<SearchOutlined />} onClick={searchNewTerms} />
                    </Input.Group>
                </Col>
                <Col span={4}>
                    <Button onClick={onClickAdd} block>Add Product</Button>
                </Col>
            </Row>
            <Row gutter={[40, 0]}>
                <Col span={24}>
                    <Table
                        columns={columns}
                        dataSource={productsData}
                        pagination={pagination}
                        loading={loading}
                        onChange={onTableChange}
                        rowKey={(record: any) => record.id}
                        onRow={(record: any, rowIndex) => {
                            return {
                              onClick: event => { openProduct(record) }
                            };
                          }}
                    />
                </Col>
            </Row>
        </div>
    )
}

export default ProductList