import { Button, Col, Form, Input, message, Row, Typography } from 'antd';
import React, { useState } from 'react';
import { useHistory } from 'react-router';
import AuthService from '../../domains/Auth/Service/AuthService';
import { notificationError } from '../../services/notification';

const { Title } = Typography;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};

const SignUp = () => {
  const [loading] = useState(false);
  const history = useHistory();

  const service = new AuthService()

  const submit = (fields: any) => {
    service.signUp(fields)
    .then((res: any) => {
      message.success(res.message);
      history.push('/auth/singin')
    })
    .catch(notificationError)
  };

  const goSignIn = () => {
    history.goBack()
  };

  return (
    <div >
      <Row gutter={[40, 0]}>
        <Col span={23}>
          <Title style={{textAlign: 'center'}} level={2}>
            Sign up
          </Title>
        </Col>
      </Row>
      <Row gutter={[40, 0]}>
        <Col span={18}>
          <Form {...layout} onFinish={submit} >
            <Form.Item name="name" label="Name"
            rules={[
              {
                required: true,
                message: 'Please input your name',
              },
              { min: 3, max: 30 }
            ]}
            >
              <Input placeholder="Please enter your name" />
            </Form.Item>
            <Form.Item name="email" label="Email" 
            rules={[
              {
                required: true,
                message: 'Please input your correct email',
                type: 'email'
              }
            ]}
            >
              <Input placeholder="Please enter your email" />
            </Form.Item>
            <Form.Item name="password" label="Password"
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please input an password'
              },
              { min: 6, max: 20, message: 'The password must contain between 6 and 20 characters' },
            ]}
            >
              <Input.Password placeholder="Please enter your email" />
            </Form.Item>
            <Form.Item name="password_confirmation" label="Confirmation"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!'
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject('The two passwords that you entered do not match!');
                },
              })
            ]}
            >
              <Input.Password placeholder="Please confirm your password" />
            </Form.Item>
            <div style={{textAlign: "center"}}>
              <Button type="primary" loading={loading} htmlType="submit">
                Save
              </Button>{' '}
              <Button htmlType="button" onClick={goSignIn} >
                Back
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </div>
  );
}

export default SignUp;
