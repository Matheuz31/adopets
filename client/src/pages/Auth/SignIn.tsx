import { Button, Col, Form, Input, message, Row, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import AuthService from '../../domains/Auth/Service/AuthService';
import { setToken, updateUser } from '../../services/Http';
import { notificationError } from '../../services/notification';

declare const window: any;
const { Title } = Typography;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};

const SignIn = () => {
  const [loading] = useState(false);
  const history = useHistory();

  const service = new AuthService()

  const submit = (fields: any) => {
    service.signIn(fields)
    .then((res:any) => {
      message.success(res.message);     
      setToken(res.token)
      updateUser(res.user)
      history.push('/dashboard/products', { username: res.user })
    })
    .catch(notificationError)
  };

  const goSignUp = () => {
    history.push('/auth/signup')
  }

  useEffect(() => {
    if (window.$token && window.$token !== '') {
      history.replace('/dashboard/products')
    }
  }, [])


  return (
    <div>
      <Row gutter={[40, 0]}>
        <Col span={23}>
          <Title style={{textAlign: 'center'}} level={2}>
            Sign In
          </Title>
        </Col>
      </Row>
      <Row gutter={[40, 0]}>
        <Col span={18}>
          <Form {...layout} onFinish={submit} >
            <Form.Item name="email" label="Email" 
            rules={[
              {
                required: true,
                message: 'Please input your correct email',
                type: 'email'
              }
            ]}
            >
              <Input placeholder="Please enter your email" />
            </Form.Item>
            <Form.Item name="password" label="Password" 
            rules={[
              {
                required: true,
                message: 'Please input your password'
              }
            ]}
            >
              <Input.Password placeholder="Please enter your password" />
            </Form.Item>
            <div style={{textAlign: "center"}}>
              <Button type="primary" loading={loading} htmlType="submit">
                Submit
              </Button>{' '}
              <Button htmlType="button" onClick={goSignUp} >
                Sign Up
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </div>
  );
}

export default SignIn;
