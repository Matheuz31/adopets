import Rest from '../../../services/Rest'

/**
 * @type {AuthService}
 */
export default class AuthService extends Rest {
    
    resource = '/auth'

    /**
     * @param {Object} params
     * @returns {Promise}
    */
    signIn (params: object) {
        return this.post('/signIn', params)
    }

    /**
     * @param {Object} params
     * @returns {Promise}
    */
    signUp (params: object) {
        return this.post('/signUp', params)
    }

    signOut () {
        return this.get('/signOut')
    }
}
