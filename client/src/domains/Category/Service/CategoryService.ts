import Rest from '../../../services/Rest'

/**
 * @type {CategoryService}
 */
export default class CategoryService extends Rest {
    
    resource = '/category'

    /**
     * @param {Object} record
     * @returns {Promise}
    */
    create (record: object) {
        return this.post('', record)
    }

    all () {
        return this.get('')
    }
}