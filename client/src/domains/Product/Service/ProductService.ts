import Rest from '../../../services/Rest'

/**
 * @type {ProductService}
 */
export default class ProductService extends Rest {
    
    resource = '/product'

    /**
     * @param {Object} record
     * @returns {Promise}
    */
    create (record: object) {
        return this.post('', record)
    }

    /**
     * @param {Object} record
     * @returns {Promise}
    */
    update (record: object) {
        return this.put(`/${this.__getId(record)}`, record)
    }

    /**
     * @param {Object} record
     * @returns {Promise}
     */
    read (record: object) {
        return this.get(`/${this.__getId(record)}`)
    }

    /**
     * @param {Object} record
     * @returns {Promise}
     */
    destroy (record: object) {
        return this.delete(`/${this.__getId(record)}`)
    
    }
    /**
     * @param {Object} record
     * @returns {Promise}
     */
    search (page: number, column: string, term = '') {
        const fragment = { params: { page, search: '' } }
        if (term !== '')  {
            fragment.params.search = `${column}=${term}`
        }

        return this.get('', fragment)
    }
}
