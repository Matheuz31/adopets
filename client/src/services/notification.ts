import { notification } from 'antd'

export const notificationError = (obj:any) => {
    notification.open({
        message: obj.message,
        description: Object.values(obj.errors).join(' '),
        duration: 10,
      });
}