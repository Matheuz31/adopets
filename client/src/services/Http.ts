import axios from 'axios'
const TOKEN_NAME = 'token'

declare const window: any;

// noinspection ES6ModulesDependencies
const standard = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
  timeout: 100000
})

export const setToken = (token: string) => {
  if (token) {
    const replace = `Bearer ${token.replace('Bearer ', '')}`
    
    updateToken(replace)
    window.$token = replace
    
    return
  }
  delete window.$token
  updateToken('')
  localStorage.removeItem(TOKEN_NAME)
}

export const updateToken = (token: string) => {
  standard.defaults.headers.common['Authorization'] = token
  localStorage.setItem(TOKEN_NAME, token)
}

export const updateUser = (user: string) => {
  window.$user = user
  localStorage.setItem('user', user)
}

/**
 * @param config
 * @returns {*}
 */
let requestOnFulfilled = function (config: any) {
  const token = window.$token

  config.headers.common['Authorization'] = token
  return config
}

/**
 * @param error
 * @returns {Promise<never>}
 */
let requestOnRejected = function (error: any) {
  // Do something with request error
  return Promise.reject(error)
}
/**
 */
standard.interceptors.request.use(requestOnFulfilled, requestOnRejected)

/**
 * @param response
 * @returns {*}
 */
let responseOnFulfilled = function (response: any) {
  // Do something with response data
  return response.data
}

/**
 * @param error
 * @returns {Promise}
 */
const responseOnRejected = function (error: any) {
  if (!error.response) {
    return Promise.reject(error)
  }

  if (error.response && [401, 403].includes(error.response.status)) {
    setToken('')
  }

  if (error.response.data) {
    return Promise.reject(error.response.data)
  }

  return Promise.reject(error)
}

standard.interceptors.response.use(responseOnFulfilled, responseOnRejected)

export default standard

