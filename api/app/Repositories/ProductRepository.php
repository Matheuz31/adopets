<?php

namespace App\Repositories;

use App\Product;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductRepository {

    /**
     * Get's a product by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($product_id)
    {
        return Product::findOrFail($product_id);
    }

    /**
     * Get's all products.
     *
     * @return mixed
     */
    public function all()
    {
        return Product::paginate(10);
    }

    /**
     * Search products by terms.
     *
     * @param array
     * @return Product
     */
    public function search($column, $search)
    {
        $products = new Product;
        return $products->where($column, 'LIKE', "%{$search}%")
        ->with([ 'category' ])->orderBy('name')->paginate(10);
    }

    /**
     * Create a product.
     *
     * @param array
     * @return Product
     */
    public function create(array $data)
    {
        $product = Product::create($data);
        Log::info("Product ". $product['id'] ." created by the user ". auth()->user()->id);

        return $product;
    }

    /**
     * Updates a product.
     *
     * @param int
     * @param array
     * @return Product
     */
    public function update($id, array $data)
    {
        try {
            Product::findOrFail($id)->update($data);
            Log::info("Product ". $id ." updated by the user ". auth()->user()->id);
            return;
        } catch (ModelNotFoundException $ex) {
            throw new \Exception ('product not found', 404);
        }
    }

    /**
     * Deletes a product.
     *
     * @param int
     */
    public function delete($id)
    {
        try {
            Product::findOrFail($id)->delete();
            Log::info("Product ". $id ." deleted by the user ". auth()->user()->id);
        } catch (ModelNotFoundException $ex) {
            throw new \Exception ('product not found', 404);
        }
    }
}
