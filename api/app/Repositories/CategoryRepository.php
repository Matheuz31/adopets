<?php

namespace App\Repositories;

use App\Category;

class CategoryRepository {

    /**
     * Get's all categorys.
     *
     * @return mixed
     */
    public function all()
    {
        return Category::all();
    }

    /**
     * Create a category.
     *
     * @param array
     * @return Category
     */
    public function create(array $data)
    {
        return Category::create($data);
    }
}
