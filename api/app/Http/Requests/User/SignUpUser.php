<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SignUpUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:30|min:3',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|max:20|min:6|confirmed'
        ];
    }

    protected function failedValidation(Validator $validator) {
        $response = ['message' => 'An error has occurred'];
        $response['errors'] = $validator->errors();

        throw new HttpResponseException(response()->json($response, 422));
    }

    // TODO: Revise messages
    public function messages()
    {
        return [
            'price.min' => 'The price must be greater than zero',
            'stock.min' => 'The stock cannot be negative'
        ];
    }
}
