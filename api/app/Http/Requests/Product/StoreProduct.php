<?php

namespace App\Http\Requests\Product;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:30|min:3',
            'description' => 'string|max:250',
            'price' => 'required|numeric|between:0.01,999999.99|regex:/^\d*(\.\d{1,2})?$/',
            'stock' => 'required|numeric|between:0,999999|regex:/^\d*(\.\d{0})?$/',
            'category_id' => 'required'
        ];
    }

    protected function failedValidation(Validator $validator) {
        $response = ['message' => 'An error has occurred'];
        $response['errors'] = $validator->errors();

        throw new HttpResponseException(response()->json($response, 422));
    }

}
