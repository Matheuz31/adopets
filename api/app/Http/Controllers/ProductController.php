<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\StoreProduct as ProductValidator;
use App\Product;
use App\Http\Resources\Product as ProductResource;
use App\Repositories\ProductRepository;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class ProductController extends Controller
{
     /**
     * @var ProductRepository
     */
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(?Request $request)
    {
        $data = '';

        if ($request->search) {
            $search = Arr::wrap(explode('=', $request->search));
            $data = $this->repository->search($search[0], $search[1]);

        } else {
            $data = $this->repository->all();
        }

        Log::info("List of products for user ". auth()->user()->id);

        return ProductResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductValidator $request)
    {
        try
        {
            $data = $request->validated();
            $product = $this->repository->create($data);


            return response(['message'  => "created product"]);
        }
        catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response(['message' => $ex->getMessage() ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->repository->find($id);

        Log::info("Product shown to the user ". auth()->user()->id);
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductValidator $request, $id)
    {
        try {
            $data = $request->validated();
            $product = $this->repository->update($id, $data);

            $response = [
                'message' => 'updated product',
            ];

            return response($response);

        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response(['message' => $ex->getMessage()], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $deleted = $this->repository->delete($id);

        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response(["message" => $ex->getMessage()], $ex->getCode());
        }

        return response([ 'message' => 'deleted product' ]);
    }
}
