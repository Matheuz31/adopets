<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\StoreCategory as CategoryValidator;
use App\Http\Resources\Category as CategoryResource;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->repository->all();
        Log::info("Categories listed for the user ". auth()->user()->id);

        return CategoryResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryValidator $request)
    {
        try
        {
            $data = $request->validated();
            $product = $this->repository->create($data);
            Log::info("Categories registered by the user ". auth()->user()->id);


            return response(['message'  => "created category"]);
        }
        catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response(['message' => $ex->getMessage() ], 500);
        }
    }
}
