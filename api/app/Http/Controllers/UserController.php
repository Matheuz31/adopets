<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Http\Requests\User\SignUpUser as SignUpUserValidator;
use App\Http\Requests\User\SignInUser as SignInUserValidator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
     /**
     * @var UserRepository
     */
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function signIn(SignInUserValidator $request)
    {
        try {
            $data = $request->validated();
            $response = $this->repository->signIn($data);

            Log::info("The user ". auth()->user()->id ." signed");

            return response($response['response'], $response['code']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response(['message' => $ex->getMessage()]);
        }
    }

    public function signUp(SignUpUserValidator $request)
    {
        $data = $request->validated();

        $user = $this->repository->create($data);

        $user->token = $user->createToken($user->email)->accessToken;

        Log::info("The user {$user->id} has been registered");

        return $user;
    }

    public function signOut()
    {
        Log::info("The user ". auth()->user()->id ." has left");
        auth()->user()->token()->revoke();
    }
}
