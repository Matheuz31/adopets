<?php

namespace App;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Uuid;

    protected $fillable = [ 'name', 'description', 'price', 'stock', 'category_id' ];

    protected $casts = [
        'price' => 'float'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
